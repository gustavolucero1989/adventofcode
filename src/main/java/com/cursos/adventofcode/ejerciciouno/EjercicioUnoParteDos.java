package com.cursos.adventofcode.ejerciciouno;

import com.cursos.adventofcode.ejerciciouno.utils.Constants;
import com.cursos.adventofcode.ejerciciouno.utils.ResultEnum;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class EjercicioUnoParteDos {
    public static void main(String[] args) {
        int score = 0;
        try (FileReader fr = new FileReader(Constants.INPUT_FILE_EJ1);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            long startTime = System.currentTimeMillis();
            while ((line = br.readLine()) != null) {
                ResultEnum me = getChosenElement(Character.toString(line.charAt(2)));
                int partialScore = getScoreFromDuel(me) + getScoreMyChosen(me, Character.toString(line.charAt(0)));
                score += partialScore;
            }
            System.out.println("Tiempo: " + (System.currentTimeMillis() - startTime) + " ms");
            System.out.println("Puntaje total: " + score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final Map<String, ResultEnum> elementMap = new HashMap<>();
    static {
        elementMap.put(Constants.X, ResultEnum.LOST);
        elementMap.put(Constants.Y, ResultEnum.DRAW);
        elementMap.put(Constants.Z, ResultEnum.WIN);
    }

    private static ResultEnum getChosenElement(String element) {
        return elementMap.getOrDefault(element, ResultEnum.NOTHING);
    }

    private static int getScoreMyChosen(ResultEnum me, String opponent) {
        if (opponent.equals(Constants.A) && me.equals(ResultEnum.LOST)) {
            return 3;
        } else if (opponent.equals(Constants.A) && me.equals(ResultEnum.DRAW)) {
            return 1;
        } else if (opponent.equals(Constants.A) && me.equals(ResultEnum.WIN)) {
            return 2;
        } else if (opponent.equals(Constants.B) && me.equals(ResultEnum.LOST)) {
            return 1;
        } else if (opponent.equals(Constants.B) && me.equals(ResultEnum.DRAW)) {
            return 2;
        } else if (opponent.equals(Constants.B) && me.equals(ResultEnum.WIN)) {
            return 3;
        } else if (opponent.equals(Constants.C) && me.equals(ResultEnum.LOST)) {
            return 2;
        } else if (opponent.equals(Constants.C) && me.equals(ResultEnum.DRAW)) {
            return 3;
        } else {
            return 1;
        }
    }

    private static int getScoreFromDuel(ResultEnum me) {
        return switch (me) {
            case WIN -> 6;
            case DRAW -> 3;
            default -> 0;
        };
    }
}