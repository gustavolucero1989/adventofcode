package com.cursos.adventofcode.ejerciciouno.utils;

public enum ResultEnum {
    WIN,
    DRAW,
    LOST,
    NOTHING;
}