package com.cursos.adventofcode.ejerciciouno.utils;

public enum ElementEnum {
    ROCK(1),
    PAPER(2),
    SCISSOR(3),
    NOTHING(0);

    private final int value;

    ElementEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}