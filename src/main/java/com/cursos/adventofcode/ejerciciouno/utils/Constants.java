package com.cursos.adventofcode.ejerciciouno.utils;

public class Constants {
    public static final String X = "X";
    public static final String Y = "Y";
    public static final String Z = "Z";
    public static final String A = "A";
    public static final String B = "B";
    public static final String C = "C";
    public static final String INPUT_FILE_EJ1 = "C:\\Users\\gusta\\Downloads\\Cursos\\adventofcode\\src\\main\\resources\\inputs\\ejercicio-uno.txt";

}
