package com.cursos.adventofcode.ejerciciouno;

import com.cursos.adventofcode.ejerciciouno.utils.Constants;
import com.cursos.adventofcode.ejerciciouno.utils.ElementEnum;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class EjercicioUnoParteUno {
    public static void main(String[] args) {
        int score = 0;
        try (FileReader fr = new FileReader(Constants.INPUT_FILE_EJ1);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            long startTime = System.currentTimeMillis();
            while ((line = br.readLine()) != null) {
                ElementEnum me = getChosenElement(Character.toString(line.charAt(2)));
                int partialScore = getScoreFromDuel(me, getChosenElement(Character.toString(line.charAt(0)))) + me.getValue();
                score += partialScore;
            }
            System.out.println("Tiempo: " + (System.currentTimeMillis() - startTime) + " ms");
            System.out.println("Puntaje total: " + score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final Map<String, ElementEnum> elementMap = new HashMap<>();
    static {
        elementMap.put(Constants.X, ElementEnum.ROCK);
        elementMap.put(Constants.A, ElementEnum.ROCK);
        elementMap.put(Constants.Y, ElementEnum.PAPER);
        elementMap.put(Constants.B, ElementEnum.PAPER);
        elementMap.put(Constants.Z, ElementEnum.SCISSOR);
        elementMap.put(Constants.C, ElementEnum.SCISSOR);
    }

    private static ElementEnum getChosenElement(String element) {
        return elementMap.getOrDefault(element, ElementEnum.NOTHING);
    }

    private static int getScoreFromDuel(ElementEnum me, ElementEnum opponent) {
        if (me.equals(opponent)) return 3;
        return switch (me) {
            case ROCK -> opponent == ElementEnum.PAPER ? 0 : 6;
            case PAPER -> opponent == ElementEnum.SCISSOR ? 0 : 6;
            case SCISSOR -> opponent == ElementEnum.ROCK ? 0 : 6;
            default -> 0;
        };
    }
}